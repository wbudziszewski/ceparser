# README #

This is library for parsing Clausewitz Engine savegame files mainly for use with Chronicle and Ironmelt programs. It is able to parse text and binary ('Ironman') files. File parsing results in creating a logical tree of nodes that can be walked through in parent-child relationships and quickly traversed to pull out necessary information.